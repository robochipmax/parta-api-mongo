const mongoose = require("mongoose");
const schema = mongoose.Schema;

const PPNSchema = new schema({
    client_id:{
        type:String,
        required:true
    },
    notiket:{
        type:String,
        required:true
    },
    nopol:{
        type:String,
        required:true
    },
    waktukeluar:{
        type:Date,
        default:Date.now
    }
});

module.exports = tlog_ppn_pasar = mongoose.model("ppn_pasar",PPNSchema);

