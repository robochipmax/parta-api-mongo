const validator = require("validator");
const isEmpty = require("../validations/is-empty");

module.exports = function validateTLogPPN(data){
    let errors = {'status':0,'msg':[]};
    console.log(data);
    data.client_id = !isEmpty(data.client_id) ? data.client_id:"" ; 
    data.notiket = !isEmpty(data.notiket) ? data.notiket:"" ; 
    data.waktukeluar = !isEmpty(data.waktukeluar) ? data.waktukeluar:"" ; 
    data.nopol = !isEmpty(data.nopol) ? data.nopol:"" ; 

    // if(!validator.isLength(data.name,{min:3,max:50})){
    //     errors.name = "Nama Harus diantara 3 dan 50 Karakter";
    // }

    // if(!validator.isLength(data.password,{min:6,max:50})){
    //     errors.password = "Password minimal 6 karakter";
    // }

    if(validator.isEmpty(data.client_id)){
        errors.msg.push("Data Client ID dibutuhkan");
    }
    
    if(validator.isEmpty(data.notiket)){
        errors.msg.push("Data notiket dibutuhkan");
    }

    if(!validator.isEmpty(data.waktukeluar)){
        errors.msg.push("Data Waktu Keluar dibutuhkan");
    }

    if(validator.isEmpty(data.nopol)){
        errors.msg.push("Data No Polisi dibutuhkan");
    }   

    // if(!validator.equals(data.password2,data.password)){
    //     errors.password2 = "Data password dan confirmed password harus sama";
    // }

    // if(validator.isEmpty(data.password2)){
    //     errors.password2 = "Data Confirm Password dibutuhkan";
    // }
    return {
        errors,
        isValid: isEmpty(errors.msg)
    }
}
