const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const app = express();
const db = require("./config/keys").mongoURI;
const tlog_ppn = require("./routes/api/ppn");
const path = require('path');

mongoose
    .connect(db, {useNewUrlParser: true})
    .then(() => console.log("mongoDB Connected"))
    .catch((err) => console.log(err));

app.use(bodyParser.urlencoded({extended : true}));
// app.use(bodyParser.json());

//routes
app.use(express.static('public'))
app.use('/api/ppn',tlog_ppn);
app.get('*', function(req, res) {
    return {status:0, msg:'Invalid URL'};
});
const port = process.env.PORT || 5000;

app.listen(port, () => console.log("server running on port "+port));
console.log(app.timeout);
app.timeout = 2000;
console.log(app.timeout);
