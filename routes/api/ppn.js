const express = require("express");
const router = express.Router();
const tlog_ppn_pasar = require("../../models/tlog_ppn_pasar");
const bcrypt = require("bcryptjs");
const validateInsertPPN = require("../../validations/validation-tlog-ppn");


router.post("/insert",(req,res) =>{
    const {errors,isValid} = validateInsertPPN(req.body);

    if(!isValid){
        return res.status(400).json(errors);
    }
    tlog_ppn_pasar.findOne({notiket: req.body.notiket})
        .then(result => {
            if(result){
                return res.status(400).json({'status':0, 'msg' : 'No Tiket sudah digunakan'});
            }else{
                const newData = new tlog_ppn_pasar({
                    client_id : req.body.client_id,
                    notiket : req.body.notiket,
                    nopol : req.body.nopol,
                    waktukeluar : req.body.waktukeluar,
                    waktumasuk : req.body.waktumasuk,
                    biaya: req.body.biaya,
                    ppn: req.body.ppn,
                    denda: req.body.denda,
                    keterangan: req.body.keterangan,
                    jnslangganan: req.body.jnslangganan,
                    lamaparkir: req.body.lamaparkir,
                    jeniskend: req.body.jeniskend,
                    kasirkeluar: req.body.kasirkeluar,
                    kasirmasuk: req.body.kasirmasuk,
                    poskeluar: req.body.poskeluar,
                });
                newData.save()
                .then(nData => res.json({'status':1,'msg':nData}))
                .catch(err => res.json({'status':0,'msg':err}))
            }
        })
});

router.get("/get",(req,res) =>{
    let start = new Date();
    tlog_ppn_pasar.aggregate([
        {
            $match: {
                thn_out: {$eq:2019}
            }
        },
        {
            $group: {
                _id: '$client_id',
                "total_biaya": {
                    $sum: "$biaya"
                },
                "total_ppn": {
                    $sum: "$ppn"
                },
                count: {
                    $sum: 1
                }
            }
        }
    ]).then( data => {
        let end  = new Date();
        let process_time = (end.getTime() - start.getTime())/1000 + "s";
        return res.status(200).json({'status':1, 'msg' : data, 'start_time': (start.getTime()/1000),'execution_time': process_time});
    }).catch(err => {
        return res.status(400).json({'status':0, 'msg' : err});
    })
});

router.get('*', function(req, res) {
    return res.status(400).json({'status':0, 'msg' : 'Invalid API PPN URL'});
});
module.exports = router;
